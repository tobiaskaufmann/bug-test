const fs = require('fs');

/*
||
XXX0
||
*/

fs.readFile('test.txt', 'utf8', (err, data) => {
    if (err) throw err;
    findBug(data);
});

startFound = false;
bugCounter = 0;

function findBug(data) {
    let lines = data.split('\n');
    // console.log(lines);

    for (let j = 0; j < lines.length; j++) {
        for (let index = 0; index < lines[j].split('').length; index++) {
            if (lines[j].split('')[index] == "|" && lines[j].split('')[index + 1] == "|") {
                if (startFound && lines[j - 2].split('')[index] == "|" && lines[j - 2].split('')[index + 1] == "|") {
                    //end found
                    //check if bug body exists
                    let bodyLine = lines[j - 1];
                    if (lines[j - 1].split('')[index] == "X"
                        && lines[j - 1].split('')[index + 1] == "X"
                        && lines[j - 1].split('')[index + 2] == "X"
                        && lines[j - 1].split('')[index + 3] == "0") {
                        console.log("FOUND: line: " + (j - 2) + " / " + j);
                        bugCounter++;
                    }
                    startFound = false;
                }
                else if (startFound) {
                    startFound = false;
                }
                startFound = true;
            }
        }
    }
    console.log("Found " + bugCounter + " bugs");
}
